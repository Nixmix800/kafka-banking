﻿namespace TransactionAnalytics.KafkaService
{
    public class WorkerConfig
    { 
        public string KafkaConsumerTopic { get; set; }
    }
}