﻿using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading;
using System.Threading.Tasks;
using Confluent.Kafka;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TransactionAnalytics.Logic;

namespace TransactionAnalytics.KafkaService
{
    public class KafkaConsumerService : IHostedService
    {
        private readonly ILogger _logger;
        
        private readonly string _kafkaConsumerTopic;
        private readonly ConsumerConfig _consumerConfig;
        private AnalyticsLogic _analyticsLogic;

        public KafkaConsumerService(ILogger<KafkaConsumerService> logger, IOptions<WorkerConfig> workerOptions,
        IOptions<List<string>> kafkaOptions, AnalyticsLogic analyticsLogic)
        {
            _logger = logger;
            _analyticsLogic = analyticsLogic;
            
            _kafkaConsumerTopic = workerOptions.Value.KafkaConsumerTopic;
            var kafkaHosts = kafkaOptions.Value;
            var bootstrapServers = string.Join(",", kafkaHosts);
            _consumerConfig = new ConsumerConfig
            {
                BootstrapServers = bootstrapServers,
                GroupId = "AnalyticsService",
                AutoOffsetReset = AutoOffsetReset.Earliest
            };
        }

        public Task StartAsync(CancellationToken cancellationToken) {
            
            try {
                using(var consumerBuilder = new ConsumerBuilder<Ignore, string> (_consumerConfig).Build()) {
                    consumerBuilder.Subscribe(_kafkaConsumerTopic);
                    var cancelToken = new CancellationTokenSource();
                    
                    _logger.LogInformation($"Now listening on topic {_kafkaConsumerTopic} on {_consumerConfig.BootstrapServers}");

                    try {
                        while (!cancelToken.IsCancellationRequested) {
                            var consumer = consumerBuilder.Consume 
                                (cancelToken.Token);
                            
                            var transaction = JsonSerializer.Deserialize 
                                <Transaction> 
                                (consumer.Message.Value);
                            
                            if (transaction != null && !string.IsNullOrEmpty(transaction.Uid))
                            {
                                _analyticsLogic.AddTransaction(transaction);
                                _logger.LogInformation($"Processing Order Id: {transaction.Uid}");
                            }
                        }
                    } catch (OperationCanceledException) {
                        consumerBuilder.Close();
                    }
                }
            } catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
            return Task.CompletedTask;
        }
        public Task StopAsync(CancellationToken cancellationToken) {
            return Task.CompletedTask;
        }
    }
}