﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TransactionAnalytics.Logic;
using Confluent.Kafka;
using Swashbuckle.AspNetCore.Annotations;

namespace TransactionAnalytics.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AnalyticsSystemController : ControllerBase
    {
        private readonly ILogger<AnalyticsSystemController> _logger;
        private readonly AnalyticsLogic _analyticsLogic;
        
        public AnalyticsSystemController(AnalyticsLogic analyticsLogic, ILogger<AnalyticsSystemController> logger)
        {
            _logger = logger;
            _analyticsLogic = analyticsLogic;
        }

        [HttpGet]
        [Route("/transactions")]
        [SwaggerOperation("GetAllTransactions")]
        [SwaggerResponse(statusCode: 200, type: typeof(Transaction), description: "Successful response")]
        [SwaggerResponse(statusCode: 400, type: typeof(Error), description: "An error occurred loading.")]
        [SwaggerResponse(statusCode: 204, type: typeof(Error), description: "No transactions registered yet.")]
        public IActionResult GetAllTransactions()
        {
            var transactions = _analyticsLogic.GetAllTransactions();

            if (!transactions.Any())
            {
                return Ok(StatusCode(204, new Error
                {
                    ErrorMessage = "No transactions registered yet."
                }));
            }

            return Ok(transactions);
        }
        
        [HttpGet]
        [Route("/transactions/count")]
        [SwaggerOperation("GetTransactionsCount")]
        [SwaggerResponse(statusCode: 200, type: typeof(Transaction), description: "Successful response")]
        [SwaggerResponse(statusCode: 400, type: typeof(Error), description: "An error occurred loading.")]
        [SwaggerResponse(statusCode: 204, type: typeof(Error), description: "No transactions registered yet.")]
        public IActionResult GetAllTransactionCount()
        {
            var transactionsCount = new TransactionCount
            {
                AllTransactionCount = _analyticsLogic.GetNumberOfTransactions(),
                LegitTransactionCount = _analyticsLogic.GetNumberOfLegitTransactions(),
                LaunderedTransactionCount = _analyticsLogic.GetNumberOfLaunderedTransactions()
            };

            if (transactionsCount.AllTransactionCount == 0)
            {
                return Ok(StatusCode(204, new Error
                {
                    ErrorMessage = "No transactions registered yet."
                }));
            }

            return Ok(transactionsCount);
        }
    }
}
