using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace TransactionAnalytics
{
    [DataContract]
    public class Transaction
    {
        [Required]
        [DataMember(Name = "uid")]
        public string Uid { get; set; }
        
        [Required]
        [DataMember(Name = "amount")]
        public int Amount { get; set; }
        
        [Required]
        [DataMember(Name = "isLaundry")]
        public bool IsLaundry { get; set; }
    }
}
