﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace TransactionAnalytics.Logic
{
    public class AnalyticsLogic
    {
        private readonly ILogger<AnalyticsLogic> _logger;
        private readonly List<Transaction> _transactions;

        public AnalyticsLogic(ILogger<AnalyticsLogic> logger)
        {
            _logger = logger;
            _transactions = new List<Transaction>();
        }

        public IEnumerable<Transaction> GetAllTransactions()
        {
            return _transactions;
        }

        public IEnumerable<Transaction> GetAllLaunderedTransactions()
        {
            return _transactions.Where(x => x.IsLaundry == true);
        }
        
        public IEnumerable<Transaction> GetAllLegitTransactions()
        {
            return _transactions.Where(x => x.IsLaundry == false);
        }
        
        public int GetNumberOfLaunderedTransactions()
        {
            return _transactions.Count(x => x.IsLaundry == true);
        }

        public int GetNumberOfLegitTransactions()
        {
            return _transactions.Count(x => x.IsLaundry == false);
        }

        public int GetNumberOfTransactions()
        {
            return _transactions.Count;
        }

        public void AddTransaction(Transaction transaction)
        {
            if (!string.IsNullOrEmpty(transaction.Uid))
            {
                _transactions.Add(transaction);
            }
        }
    }
}