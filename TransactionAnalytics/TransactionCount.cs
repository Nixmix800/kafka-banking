﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace TransactionAnalytics
{
    [DataContract]
    public class TransactionCount
    {
        [Required]
        [DataMember(Name = "transactionCount")]
        public int AllTransactionCount { get; set; }
        
        [Required]
        [DataMember(Name = "legitTransactionCount")]
        public int LegitTransactionCount { get; set; }
        
        [Required]
        [DataMember(Name = "launderedTransactionCount")]
        public int LaunderedTransactionCount { get; set; }
    }
}