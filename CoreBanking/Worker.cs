using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using Confluent.Kafka;
using System.Net;
using System.Text.Json;

namespace CoreBanking
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly int _execInterval;
        private readonly string _kafkaTopic;
        private readonly ProducerConfig _producerConfig;

        public Worker(ILogger<Worker> logger, IOptions<WorkerConfig> workerOptions,
            IOptions<List<string>> kafkaOptions)
        {
            _logger = logger;
            _execInterval = workerOptions.Value.ExecutionInterval;
            _kafkaTopic = workerOptions.Value.KafkaTopic;
            var kafkaHosts = kafkaOptions.Value;
            
            var bootstrapServers = string.Join(",", kafkaHosts);
            
            _producerConfig = new ProducerConfig
            {
                BootstrapServers = bootstrapServers,
                ClientId = Dns.GetHostName(),
            };
        }
        
        private void Handler(DeliveryReport<Null, string> report)
        {
            if (report.Error.IsError) 
            {
                _logger.LogError(report.Error.Reason);
            }
            else
            {
                _logger.LogDebug($"Topic: {report.Topic}, Value: {report.Value}");
            }
        }
        
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            using var producer = new ProducerBuilder<Null, string>(_producerConfig).Build();
            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);

                var transaction = GenerateRandomTransaction(0, 1500, true);
                var test = JsonSerializer.Serialize(transaction);
                
                await producer.ProduceAsync(_kafkaTopic, new Message<Null, string> { Value = JsonSerializer.Serialize(transaction)} );
                await Task.Delay(_execInterval, stoppingToken);
            }
        }

        private static Transaction GenerateRandomTransaction(int startValue, int endValue, bool negativePossible)
        {
            var rand = new Random();
            var amount = rand.Next(startValue, endValue + 1);
            
            if (negativePossible)
            {
                if(rand.Next(2) == 1)
                {
                    amount = -amount;
                }
            }
            var transaction = new Transaction()
            {
                Amount = amount,
                Uid = Guid.NewGuid().ToString()
            };

            return transaction;
        }
    }
}
