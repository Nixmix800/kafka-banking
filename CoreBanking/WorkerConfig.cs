﻿namespace CoreBanking
{
    public class WorkerConfig
    {
        public int ExecutionInterval { get; set; }
        public string KafkaTopic { get; set; }
    }
}