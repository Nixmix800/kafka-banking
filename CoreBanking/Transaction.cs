﻿namespace CoreBanking
{
    public class Transaction
    {
        public string Uid { get; set; }
        public int Amount { get; set; }
    }
}