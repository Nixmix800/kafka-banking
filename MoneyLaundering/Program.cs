using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace MoneyLaundering
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    var configuration = new ConfigurationBuilder()
                        .AddJsonFile("appsettings.json") // Duplicate == Json1
                        .AddEnvironmentVariables() // Duplicate == Environment
                        .Build();
                    services.Configure<WorkerConfig>(configuration.GetSection(nameof(WorkerConfig)));
                    services.Configure<List<string>>(configuration.GetSection("KafkaConfig"));
                    services.AddHostedService<Worker>();
                });
    }
}
