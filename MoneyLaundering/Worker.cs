using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Confluent.Kafka;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace MoneyLaundering
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly int _execInterval;
        private readonly string _kafkaConsumerTopic;
        private readonly string _kafkaProducerTopic;
        private readonly ProducerConfig _producerConfig;
        private readonly ConsumerConfig _consumerConfig;

        public Worker(ILogger<Worker> logger, IOptions<WorkerConfig> workerOptions,
            IOptions<List<string>> kafkaOptions)
        {
            _logger = logger;
            _execInterval = workerOptions.Value.ExecutionInterval;
            _kafkaConsumerTopic = workerOptions.Value.KafkaConsumerTopic;
            _kafkaProducerTopic = workerOptions.Value.KafkaProducerTopic;
            var kafkaHosts = kafkaOptions.Value;
            
            var bootstrapServers = string.Join(",", kafkaHosts);
            
            _consumerConfig = new ConsumerConfig
            {
                BootstrapServers = bootstrapServers,
                GroupId = "MoneyLaundringService",
                AutoOffsetReset = AutoOffsetReset.Earliest
            };
            
            _producerConfig = new ProducerConfig
            {
                BootstrapServers = bootstrapServers,
                ClientId = Dns.GetHostName()
            };
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            using var producer = new ProducerBuilder<Null, string>(_producerConfig).Build();
            using var consumer = new ConsumerBuilder<Ignore, string>(_consumerConfig).Build();
            
            consumer.Subscribe(_kafkaConsumerTopic);
            
            while (!stoppingToken.IsCancellationRequested)
            {
                var consumerResult = consumer.Consume(stoppingToken);
                
                if (consumerResult.Message.Value != string.Empty)
                {
                    _logger.LogInformation(consumerResult.Message.Value);
                    var transaction = new Transaction();
                    try
                    {
                        transaction = JsonSerializer.Deserialize<Transaction>(consumerResult.Message.Value);
                        if (IsLaundered(transaction))
                        {
                            _logger.LogInformation($"Transaction {transaction.Uid} with amount {transaction.Amount} is marked as laundry.");
                            transaction.IsLaundry = true;
                        }
                    }
                    catch (Exception e)
                    {
                        _logger.LogError(e.Message);
                    }
                    
                    await producer.ProduceAsync(_kafkaProducerTopic, new Message<Null, string> { Value = JsonSerializer.Serialize(transaction)}, stoppingToken);
                }

                _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);
                await Task.Delay(_execInterval, stoppingToken);
            }

            consumer.Close();
        }

        private bool IsLaundered(Transaction transaction)
        {
            if (Math.Abs(transaction.Amount) > 1000)
            {
                return true;
            }
            return false;
        }
    }
}
