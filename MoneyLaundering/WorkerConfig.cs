﻿namespace MoneyLaundering
{
    public class WorkerConfig
    {
        public int ExecutionInterval { get; set; }
        public string KafkaConsumerTopic { get; set; }
        public string KafkaProducerTopic { get; set; }
    }
}