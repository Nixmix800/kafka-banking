﻿namespace MoneyLaundering
{
    public class Transaction
    {
        public string Uid { get; set; }
        public int Amount { get; set; }
        public bool IsLaundry { get; set; }
    }
}